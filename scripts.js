function createNewUser() {
    const userFirstName = prompt("What is your name?");
    const userLastName = prompt("What is your last name?");
    const userBirthday = prompt("When your birthday is? (DD.MM.YYYY)");


    const newUser = {
        firstName: userFirstName,
        lastName: userLastName,

        getLogin: function () {
            return this["firstName"].toLowerCase().slice(0,1) + this["lastName"].toLowerCase();
        },

        getAge: function (){
            const [day, month, year] = userBirthday.split('.');
            const birthdayConverted = new Date(+year, month - 1, +day);
            const ageDifMs = Date.now() - new Date(birthdayConverted).getTime();
            const ageDate = new Date(ageDifMs);
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        },

        getPassword: function (){
            return this["firstName"].toUpperCase().slice(0, 1) + this["lastName"].toLowerCase() +
                userBirthday.slice(-4);
        },

        setFirstName(newValue) {
            Object.defineProperty(this, "firstName", {
                value: newValue,
                writable: false,
            });
        },

        setLastName(newValue) {
            Object.defineProperty(this, "lastName", {
                value: newValue,
                writable: false,
            });
        },
    };

    Object.defineProperty(newUser, "firstName", {
        configurable: true,
        writable: false,
    });

    Object.defineProperty(newUser, "lastName", {
        configurable: true,
        writable: false,
    });

    return newUser;
}

let user = createNewUser();

console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());


